const mongoose = require('mongoose');

const mainCategoryTranslationSchema = new mongoose.Schema({
    language: {
        type: String,
        required: [true, 'Please tell us the product name!']
    },
    text: {
        type: String,
        required: [true, 'Please tell us the product name!']
    },
});

const mainCategoryTranslation = mongoose.model('mainCategoryTranslation', mainCategoryTranslationSchema);

module.exports = {
    mainCategoryTranslation,
    mainCategoryTranslationSchema
}