const mongoose = require('mongoose');

const marketingNameTranslationSchema = new mongoose.Schema({
    language: {
        type: String,
        required: [true, 'Please tell us the language']
    },
    text: {
        type: String,
        required: [true, 'Please tell us the text']
    },
});

const marketingNameTranslation = mongoose.model('marketingNameTranslation', marketingNameTranslationSchema);

module.exports = {
    marketingNameTranslation,
    marketingNameTranslationSchema
}