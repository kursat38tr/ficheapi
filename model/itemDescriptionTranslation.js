const mongoose = require('mongoose');

const itemDescriptionTranslationSchema = new mongoose.Schema({
    language: {
        type: String,
        required: [true, 'Please tell us the product name!']
    },
    text: {
        type: String,
        required: [true, 'Please tell us the product name!']
    },
});

const itemDescriptionTranslation = mongoose.model('itemDescriptionTranslation', itemDescriptionTranslationSchema);

module.exports = {
    itemDescriptionTranslation,
    itemDescriptionTranslationSchema
};
