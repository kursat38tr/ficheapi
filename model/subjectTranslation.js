const mongoose = require('mongoose');

const subjectTranslationSchema = new mongoose.Schema({
    language: {
        type: String,
        required: [true, 'Please tell us the product name!']
    },
    text: {
        type: String,
        required: [true, 'Please tell us the product name!']
    },
});

const subjectTranslation = mongoose.model('subjectTranslation', subjectTranslationSchema);

module.exports = {
    subjectTranslation,
    subjectTranslationSchema
}