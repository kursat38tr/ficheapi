const mongoose = require('mongoose');

const ingredientItemSchema = new mongoose.Schema({
    id: {
        type: Number,
        required: [true, 'Please tell us the id']
    },
});

const ingredientItem = mongoose.model('ingredientItem', ingredientItemSchema);

module.exports = {
    ingredientItem,
    ingredientItemSchema
}