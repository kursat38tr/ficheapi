const mongoose = require('mongoose');

const subCategoryTranslationSchema = new mongoose.Schema({
    language: {
        type: String,
        required: [true, 'Please tell us the product name!']
    },
    text: {
        type: String,
        required: [true, 'Please tell us the product name!']
    },
});

const subCategoryTranslation = mongoose.model('subCategoryTranslation', subCategoryTranslationSchema);

module.exports = {
    subCategoryTranslation,
    subCategoryTranslationSchema
}