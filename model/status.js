const mongoose = require('mongoose');

const statusSchema = new mongoose.Schema({
    statusId: {
        type: Number,
        required: [true, 'Please tell us the product name!']
    },
    itemId: {
        type: Number,
        required: [true, 'Please tell us the product name!']
    },
    status: {
        type: String,
        required: [true, 'Please tell us the product name!']
    },
    validFrom: {
        type: String,
        required: [true, 'Please tell us the product name!']
    },
    validTo: {
        type: String,
        required: [true, 'Please tell us the product name!']
    },
});

const Status = mongoose.model('status', statusSchema);

module.exports = {
    Status,
    statusSchema
};
