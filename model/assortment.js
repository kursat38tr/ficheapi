const mongoose = require('mongoose');

const {subCategoryTranslationSchema} = require("./subCategoryTranslation");
const {subjectTranslationSchema} = require("./subjectTranslation");
const {mainCategoryTranslationSchema} = require("./mainCategoryTranslation");


const assortmentSchema = new mongoose.Schema({
    id: {
        type: Number,
        required: [true, 'Please tell us the id!']
    },
    itemId: {
        type: Number,
        required: [true, 'Please tell us the itemId!']
    },
    type: {
        type: String,
        required: [true, 'Please tell us the type!']
    },
    mainCategoryId: {
        type: Number,
        required: [true, 'Please tell us the mainCategoryId!']
    },
    mainCategory: {
        type: String,
        required: [true, 'Please tell us the mainCategory!']
    },
    mainCategoryTranslation: [mainCategoryTranslationSchema],
    subCategoryId: {
        type: Number,
        required: [true, 'Please tell us the subCategoryId!']
    },
    subCategory: {
        type: String,
        required: [true, 'Please tell us the subCategory!']
    },
    subCategoryTranslation: [subCategoryTranslationSchema],
    subjectId: {
        type: Number,
        required: [true, 'Please tell us the subjectId!']
    },
    subject: {
        type: String,
        required: [true, 'Please tell us the subject!']
    },
    subjectTranslation: [subjectTranslationSchema]
});

const assortment = mongoose.model('assortment', assortmentSchema);

module.exports = {
    assortment,
    assortmentSchema
}