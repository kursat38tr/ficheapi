const catchAsync = require('../errors/catchAsync');
const Item = require('../model/item');
// const Main = require("../model/main");
// const fs = require("fs");
// const Main = require("../model/main");

// const item = JSON.parse(
//     fs.readFileSync(`${__dirname}/../file/jsonfile.json`)
// );

exports.getAllItems = catchAsync(async (req, res) => {
    const getItems = await Item.find();
    res.status(200).json({
        status: 'success',
        results: getItems.length,
        data: {
            getItems
        }
    });
});


exports.createItem = catchAsync(async (req, res) => {

    const newItem = await Item.create(req.body);
    console.log(newItem);
    res.status(201).json({
        status: 'success',
        data: {
            item: newItem
        }
    })
});
// app.post('/api/v1/item', (req, res)=> {
//     // console.log(req.body);
//
//     const newId = item[item.length -1].id + 1;
//     const newItem = Object.assign({id: newId}, req.body);
//
//     item.push(newItem);
//     fs.writeFile(`${__dirname}/file/jsonfile.json`, JSON.stringify(item), err => {
//         res.status(201).json({
//             status: 'Success',
//             data: {
//                 item: newItem
//             }
//         }) ;
//     })
// })