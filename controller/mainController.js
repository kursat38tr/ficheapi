const catchAsync = require('../errors/catchAsync');
const {Main} = require('../model/main');

exports.createMain = catchAsync(async (req, res) => {
    console.log(req.body);
    const newMain = await Main.create(req.body);

    res.status(201).json({
        status: 'success',
        data: {
            main: newMain
        }
    })
});

exports.getAllMain = catchAsync(async (req, res) => {
    const main = await Main.find().populate('status');

    res.status(200).json({
        status: 'success',
        result: main.length,
        data: {
            main
        }
    });
});

exports.getMain = catchAsync(async (req, res) => {
    const main = await Main.findById(req.params.id);

    res.status(200).json({
        status: 'success',
        data: {
            main
        }
    });
});

exports.updateMain = catchAsync(async (req, res) =>{

    const main = Main.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true
    });

    res.status(200).json({
        status: 'success',
        data: {
            main
        }
    })
});

exports.deleteMain = catchAsync(async (req, res) => {

    const main = await Main.findByIdAndDelete(req.params.id);

    res.status(204).json({
        status: 'success',
        data: {
            main
        }
    })
});