const catchAsync = require('../errors/catchAsync');
const {Status} = require('../model/status')


// exports.getAllItems = catchAsync(async (req, res) => {
//
//     res.status(200).json({
//         status: 'success',
//         results: item.length,
//         data: {
//             item
//         }
//     });
// });

exports.createStatus = catchAsync(async (req, res) => {

    const newStatus = await Status.create(req.body);

    res.status(201).json({
        status: 'success',
        data: {
            tour: newStatus
        }
    })
});

exports.getAllStatus = catchAsync(async (req, res) => {
    const main = await Status.find();

    res.status(200).json({
        status: 'success',
        result: main.length,
        data: {
            main
        }
    });
});

exports.getStatus = catchAsync(async (req, res) => {
    const main = await Status.findById(req.params.id);

    res.status(200).json({
        status: 'success',
        data: {
            main
        }
    });
});

// app.post('/api/v1/item', (req, res)=> {
//     // console.log(req.body);
//
//     const newId = item[item.length -1].id + 1;
//     const newItem = Object.assign({id: newId}, req.body);
//
//     item.push(newItem);
//     fs.writeFile(`${__dirname}/file/jsonfile.json`, JSON.stringify(item), err => {
//         res.status(201).json({
//             status: 'Success',
//             data: {
//                 item: newItem
//             }
//         }) ;
//     })
// })