const express = require('express');
const main = require('../controller/mainController');


const router = express.Router();

router.post("/create", main.createMain);
router.get("/getAll", main.getAllMain);
router.post("/find/:id", main.getMain);
router.patch("/update/:id", main.updateMain);
router.delete("/delete/:id", main.deleteMain);

// router
//     .route('/:id')
//     .get("/getAllMain", main.getAllMain)

module.exports = router;