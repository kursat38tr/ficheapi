const express = require('express');
const status = require("../controller/statusController");

const router = express.Router();

router.post("/create", status.createStatus);
router.get("/find", status.getAllStatus);
router.post("/find/:id", status.getStatus);

module.exports = router;